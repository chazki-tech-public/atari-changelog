# ATARI-CHANGELOGS

`Added`: Nuevos features.\
`Changed`: Cambios en la funcionalidad actual.\
`Deprecated`: Features que serán removidos en futuros releases.\
`Removed`: Features removidos en este release.\
`Fixed`: Bugs solucionados.\
`Security`: Cambios respecto a la seguridad.
`Release`: Se desplegó la versión a producción.

## 2.2.0
`Added`
- Agregadas mejoras en el módulo WARD. Ahora se pueden visualizar dos rutas al mismo tiempo y combinarlas en una sola.

## 2.1.7
`Fixed`
- Bug que provocaba que la fecha y hora de recojo de órdenes con el mismo punto de pickup, en rutas donde se recogían al mismo tiempo, fuera incorrecta.

## `Release` 2.1.4
`Fixed`
- Bug que provocaba que las queries de reporte de finanzas 1 y 6, no se ejecutaran por exceder el máximo de memoria por default.

## 2.1.3
`Changed`
- Se modificaron las operaciones internas que realiza el reporte de finanzas 1 para aumentar su eficacia al generar reportes.

`Fixed`
- Bug que provocaba doble scroll en la pantalla de _seguimiento de paquetes_ al hacer un resize de la ventana.

## 2.1.2
`Fixed`
- Bug de Bata, no se mostraban los estados porque no tenían _group order_.

## 2.1.1
`Fixed`
- Los track codes ahora se guardan trimeados en la base de datos, para prevenir que se almacenen track codes que tengan espacios en blanco a los extremos.

## 2.1.0
`Added`
- Se añadió las notificaciones para ripley para 3 estados: COMPLETED, FAILED_PICK y FAILED.  

## `Release`2.0.1

`Changed`
- El reporte de finanzas RP6 fue modificado para agregar los campos ARRIVED, ARRIVED_D, Seguro y reordenar algunos campos.\ 
- El reporte de operaciones RP7 fue modificado.

`Fixed`
- Se solucionó un error que causaba que las distancias entre dos puntos en una órden no se guardaran correctamente, 
lo que obligaba a ser calculado cada vez que se requería.\
- Se solucionó un problema que no permitía a la pantalla de seguimiento, mostrar correctamente la posición del motorizado.\
- Se solucionó un problema que no permitía mostrar los datos correctamente en la pantalla de 'Mapa de Calor'
